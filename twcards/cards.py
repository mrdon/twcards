from enum import Enum
import random
import readline
from termcolor import cprint


class Phase(Enum):
    Play_Minions = 1
    Play_Actions = 2
    Battle = 3
    Draw = 4


class MinionCard(object):
    def __init__(self, name, attack, defense, cost):
        self.cost = cost
        self.defense = defense
        self.attack = attack
        self.name = name
        self.default_attack = attack
        self.default_defense = defense
        self.reset()

    def reset(self):
        self.attack = self.default_attack
        self.defense = self.default_defense

    def take_damage(self, damage):
        self.defense = max(0, self.defense - damage)
        extra = ""
        if self.dead:
            extra = "and is dead "
        cprint('<<< Card %s takes %s damage %s>>>' % (self.name, damage, extra), 'red')

    @property
    def dead(self):
        return self.defense <= 0

    def __str__(self, *args, **kwargs):
        return "{name} - ATT: {attack} DEF: {defense} COST: {cost}".format(
            name=self.name,
            attack=self.attack,
            defense=self.defense,
            cost=self.cost
        )


class ActionCard(object):
    def __init__(self, faction, name, cost):
        self.cost = cost
        self.name = name
        self.faction = faction


class EnergyCard(object):
    def __init__(self, value):
        self.value = value

    def __str__(self, *args, **kwargs):
        return "Energy {value}".format(value=self.value)


class Deck(object):
    def __init__(self, cards=None, shuffle=True, face_up=False):
        if not cards:
            cards = []
        self._cards = cards
        if shuffle and self._cards:
            random.shuffle(self._cards)
        self.face_up = face_up

    def __len__(self):
        return len(self._cards)

    def add(self, card):
        self._cards.append(card)
        return self

    def draw(self, count=1):
        if len(self._cards) < count:
            count = len(self._cards)

        cards = self._cards[-1 * count:]
        self._cards = self._cards[:-1 * count]
        return cards


class Player(object):
    def __init__(self, name):
        self.name = name
        self.deck = Deck(
            [MinionCard(x, x, x, x) for x in [1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 6, 7, 8]] + \
            [EnergyCard(1) for _ in range(10)])

        self.hand = self.deck.draw(5)
        self.discard = Deck(face_up=True)
        self.health = 20

    def play_card(self, card):
        self.hand.remove(card)

    def draw(self, num_cards):
        self.hand += self.deck.draw(num_cards)

    def discard_card(self, card):
        self.discard.add(card)

    def notify_illegal_move(self, move):
        raise Exception()

    def next_phase_minions(self, board):
        raise Exception()

    def take_damage(self, damage):
        self.health = max(0, self.health - damage)

    @property
    def dead(self):
        return self.health <= 0


class ConsolePlayer(Player):
    def notify_illegal_move(self, move):
        cprint('<<< Illegal move: %s >>>' % move, 'red', attrs=['bold'])

    def next_phase_minions(self, board):
        energy_casted = False
        while True:
            self.print_board(board)
            print("Cards \t%s" % "\n\t".join(["%s: %s" % (x, card) for x, card in enumerate(self.hand)]))
            if not energy_casted and "y" == input("Would you like to play an energy card?"):
                card_pos = int(input("Which card would you like to play?"))
                card = self.hand[card_pos]
                yield AddEnergy(self, card)
                energy_casted = True
            elif "y" == input("Would you like to play an minion card?"):
                card_pos = int(input("Which card would you like to play?"))
                line_pos = int(input("Which battle line position would you like to play it to?"))
                card = self.hand[card_pos]
                yield AddToBattleLine(self, card, line_pos)
            else:
                break

    def print_board(self, board):
        print("Phase: play minions and energy cards\n")

        def print_player(player):
            print("Player {player}\tHealth: {health} Energy: {total}/{remaining}\tDeck: {deck_size} cards".format(
                health=player.health,
                player=player.name,
                remaining=board.energy_remaining[player],
                total=board.energy_total[player],
                deck_size=board.deck_sizes[player]
            ))

        def print_battle_line(player):
            battle_line = board.battle_lines[player]
            print("\t".join(["[{name} {attack}/{defense}]".format(
                name=card.name, attack=card.attack, defense=card.defense) if card else "[Empty]" for card in battle_line]))

        player2 = next(player for player in board.battle_lines.keys() if self != player)
        print_player(player2)
        print('-' * 60)
        print_battle_line(player2)
        print_battle_line(self)
        print('-' * 60)
        print_player(self)

    def take_damage(self, damage):
        super().take_damage(damage)
        cprint('<<< Player %s takes %s damage >>>' % (self.name, damage), 'red')


class Board(object):
    def __init__(self, players):
        self.battle_lines = {}
        self.deck_sizes = {}
        self.hand_sizes = {}
        self.energy_total = {}
        self.energy_remaining = {}
        self.current_phase = None

        for x, player in enumerate(players):
            self.battle_lines[player] = [None] * 6
            self.energy_total[player] = 0
            self.energy_remaining[player] = 0
            self.update(player)

    def update(self, player):
        self.deck_sizes[player] = len(player.deck)
        self.hand_sizes[player] = len(player.hand)

    def add_to_battle(self, player, pos, card):
        self.battle_lines[player][pos] = card
        self.energy_remaining[player] -= card.cost

    def add_energy(self, player, value):
        self.energy_remaining[player] += value
        self.energy_total[player] += value

    def notify_battle(self):
        cprint("====== Battle =======", 'red', 'on_yellow')

    def new_round(self, round):
        for player, energy in self.energy_total.items():
            self.energy_remaining[player] = energy

        cprint("====== Round %s =======" % round, 'white', 'on_blue')
        print('')

    def winner(self, player):
        cprint("====== Player %s Won! =======" % player.name, 'white', 'on_green', attrs=['bold'])

    def remove_minion(self, player, pos):
        self.battle_lines[player][pos] = None

class Move(object):

    valid_phase = None
    one_per_round = False

    def is_valid(self, board):
        if board.current_phase != self.valid_phase:
            return False

        return True

    def execute(self, board):
        pass


class AddToBattleLine(Move):
    def __init__(self, player, card, pos):
        self.pos = int(pos)
        self.card = card
        self.player = player
        self.valid_phase = Phase.Play_Minions

    def is_valid(self, board):
        if not super().is_valid(board):
            return False
        if not isinstance(self.card, MinionCard):
            return False
        if board.battle_lines[self.player][self.pos]:
            return False
        if board.energy_remaining[self.player] < self.card.cost:
            return False
        return True

    def execute(self, board):
        self.player.play_card(self.card)
        board.add_to_battle(self.player, self.pos, self.card)

    def __str__(self, *args, **kwargs):
        return "Add %s to battle line" % self.card


class AddEnergy(Move):
    one_per_round = True
    def __init__(self, player, card):
        self.card = card
        self.player = player
        self.valid_phase = Phase.Play_Minions

    def is_valid(self, board):
        if not super().is_valid(board):
            return False
        if not isinstance(self.card, EnergyCard):
            return False
        return True

    def execute(self, board):
        self.player.play_card(self.card)
        board.add_energy(self.player, self.card.value)

    def __hash__(self):
        return self.__class__.__name__.__hash__()

    def __eq__(self, other):
        return self.__class__.__name__ == other.__class__.__name__

    def __ne__(self, other):
            return self.__class__.__name__ != other.__class__.__name__

    def __str__(self, *args, **kwargs):
        val = ''
        if hasattr(self.card, 'value'):
            val = self.card.value
        return "Add %s energy" % val


class Game(object):
    def __init__(self):
        self.players = [ConsolePlayer("One"), ConsolePlayer("Two")]
        self.board = Board(self.players)

    def start(self):
        round = 0
        while True:
            round += 1
            self.board.new_round(round)
            self.phase_minions()
            # self.phase_actions()
            winner = self.phase_battle()
            if winner:
                self.board.winner(winner)
                break
            self.phase_draw()

    def phase_minions(self):
        self.board.current_phase = Phase.Play_Minions
        for player in self.players:
            moves = set()
            for move in player.next_phase_minions(self.board):
                if move.is_valid(self.board):
                    if move.one_per_round and move in moves:
                        player.notify_illegal_move(move)
                    else:
                        moves.add(move)
                        move.execute(self.board)
                else:
                    player.notify_illegal_move(move)
                self.board.update(player)

    def phase_draw(self):
        self.board.current_phase = Phase.Draw
        for player in self.players:
            cards_to_draw = min(0, 5 - self.board.hand_sizes[player]) + 1
            player.draw(cards_to_draw)
            self.board.update(player)

    def phase_battle(self):
        self.board.current_phase = Phase.Battle

        self.board.notify_battle()

        for x in range(len(self.board.battle_lines[self.players[0]])):
            card1 = self.board.battle_lines[self.players[0]][x]
            card2 = self.board.battle_lines[self.players[1]][x]

            if card1 and card2:
                card1 = self.board.battle_lines[self.players[0]][x]
                card1.take_damage(card2.attack)
                if card1.dead:
                    self.board.remove_minion(self.players[0], x)
                    self.players[0].discard_card(card1)
                    self.board.update(self.players[0])

                card2 = self.board.battle_lines[self.players[1]][x]
                card2.take_damage(card1.attack)
                if card2.dead:
                    self.board.remove_minion(self.players[1], x)
                    self.players[1].discard_card(card2)
                    self.board.update(self.players[1])

            elif card1:
                self.players[1].take_damage(card1.attack)
                self.board.update(self.players[1])
                if self.players[1].dead:
                    return self.players[0]
            elif card2:
                self.players[0].take_damage(card2.attack)
                self.board.update(self.players[0])
                if self.players[0].dead:
                    return self.players[1]


if __name__ == "__main__":
    game = Game()

    game.start()













